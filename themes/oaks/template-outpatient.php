<?php
/*
Template Name: Outpatient
*/
?>
<?php get_header(); ?>
<section class="banner outpatient">
	<div class="row vert-pad">
		<div style="max-width: 700px; margin: 0 auto; float: none;" class="large-12 columns text-center vert-pad-large">
			<h1>Outpatient</h1>
			<p>Foundations Memphis is a group-oriented program that offers staff and peer support, psychiatric evaluation and assessment, medication management, group therapy and life skills classes integrated with 12-Step principles.</p>			
		</div>
		<!-- <div class="large-6 columns">
			<div class="video-box horz-marg-small border">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/d8glcxYhWnA?rel=0&amp;showinfo=0;controls=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">					
					<p class="text-center">Gregory Hughes Discusses Foundations Memphis</p>
				</div>				
			</div>
		</div> -->
	</div>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-7 columns">
			<h2 class="text-center">Evidence-Based Treatment</h2>
			<p>Using an evidence-based integrated treatment model, we serve patients stepping down from higher intensity services as well as individuals seeking entry-level outpatient care. Every patient receives an individualized program to meet his or her unique needs, created by a staff with expertise in treating co-occurring addiction and mental health disorders.
			</p>
			<div class="text-center">
				<a href="<?php echo get_site_url(); ?>/outpatient-services/iop-for-co-occurring-disorders/" class="button small round">Learn More</a>
			</div>
		</div>
		<div class="large-5 columns vert-pad-small">
			<div class="text-center">
				<ul class="medium-block-grid-3 clearing-thumbs clearing-feature" data-clearing>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-front.jpg" alt="" class="th"></li>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-outpatient11.jpg" alt="" class="th hide-for-small"></li>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/DSC_6391-reduced.jpg" alt="" class="th hide-for-small"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-outpatient21.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-outpatient31.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-outpatient4.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-outpatient-conference-room.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-outpatient-office2.jpg" alt="" class="th"></li>					
				</ul> 
				<a href="<?php echo get_site_url(); ?>/outpatient-services/facility-photos" class="button outline small round vert-marg-tiny">Photo Tour</a>
			</div>	
		</div>
	</div>
</section>

<section>
	<div class="row">
		<div class="large-8 columns">
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Co-occurring
						Disorders
					</h2>
				</div>
				<div class="large-9 columns">					
					<div class="box">
						<p>Co-occurring disorders affect more than 10 million Americans each year and describe a condition where a person is affected by both chemical dependency and an emotional or psychiatric issue ranging from depression or bipolar disorder to PTSD or anxiety. Foundation Memphis Intensive Outpatient Program for Co-occurring Disorders addresses both through an integrated treatment model, which provides the best opportunity for lasting recovery.</p>					
					</div>
				</div>
			</article>
			<div class="vert-pad">
				<h2 class="text-center">
					Foundation Memphis Outpatient Staff 
				</h2>
				<p class="text-center">Our experienced and compassionate team will help you or your loved one find hope at various points along the journey to recovery. We accommodate each patient at his or her particular stage of treatment and recovery, always focusing on that persons’ particular needs. </p>
				<ul class="medium-block-grid-6 small-block-grid-2 staff">
					<?php $the_query = new WP_Query( 'post_type=outpatient_staff&posts_per_page=6&outpatient_staff_location=outpatient' );
					while ( $the_query->have_posts() ) : $the_query->the_post();?>
					 <li class="text-center">
					 	<div class="headshot">
					 	<?php if ( has_post_thumbnail() ) {
				    	$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' );
				     	echo '<img width="100%" height="100%" src="' . $image_src[0] . '">';
						} ?>
						</div>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>		
				</ul>	
				<div class="text-center">
					<a href="<?php echo get_site_url(); ?>/outpatient-services/outpatient-staff" class="button small round">Meet the Outpatient Team</a>
				</div>
			</div>	
			<div class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Where We're
						Located
					</h2>
				</div>
				<div class="large-6 columns">
					<div class="box">
						<div class="flex-video"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3264.1812399342953!2d-89.872352!3d35.102185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x887f84eb0afa58f9%3A0x7864ec51ece5044d!2sFoundations+Memphis!5e0!3m2!1sen!2sus!4v1425318400340" width="600" height="450" frameborder="0" style="border:0"></iframe></div>
					</div>
				</div>
				<div class="large-3 columns vert-pad mobile-center" itemscope itemtype="http://schema.org/Organization">
					<h4 itemprop="name">Foundations Memphis</h4>
					<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<div itemprop="streetAddress">1083 W Rex Rd</div>
						<span itemprop="addressLocality">Memphis, TN</span> <span itemprop="postalCode">38119</span>
						<div itemprop="telephone"><?php echo do_shortcode('[frn_phone number="901.505.6519"]'); ?></div>
						<img class="mobile-hidden" itemprop="logo" alt="Foundations Memphis" src="/wp-content/uploads/Foundations-Memphis-TN-2.jpg" style="width:100px;" />
					</p>
				</div>
			</div>		 
		</div>
		<aside class="large-4 columns">
			<div class="box-sq-light">
				<h3 class="text-center">Treatment Services</h3>
				<ul>
					<li>An individualized treatment plan focused on the unique needs of each patient</li>
					<li>Focus on co-occurring disorders</li>
					<li>Comprehensive psychiatric assessment</li>
					<li>Specialty groups focused on education and increased knowledge of healthy recovery</li>
					<li>Primary group therapy with a licensed mental health professional</li>
					<li>Full panel drug screening upon admission and periodic random drug testing</li>
					<li>Motivational Interviewing using stages-of-change approach</li>
					<li>Physician-directed medication management</li>
					<li>Educational programs and lectures</li>
					<li>Family programming, including the Family Lecture Series and Intensive Family Workshops</li>
				</ul>
				<h3 class="text-center">Features & Ammenities</h3>
				<ul>
					<li>Convenient location in suburban East Memphis</li>
					<li>Day and evening programs</li>
					<li>Compassionate care in a non-judgmental setting</li>
					<li>Holistic interventions including meditation and hypnotherapy</li>
					<li>We provide treatment for those taking Suboxone or Subutex</li> 
				</ul>
				<div class="telephone-cta">
					<p>Get Started Today</p>
					<span class="number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sidebar" number="901.505.6519"]'); ?></span>
					<div class="text-center">
						<a href="<?php echo get_site_url(); ?>/contact" class="button outline small round vert-marg-tiny">Contact Us</a>
					</div>
				</div>			
			</div>
		</aside>
	</div>
</section>
<?php get_footer(); ?>