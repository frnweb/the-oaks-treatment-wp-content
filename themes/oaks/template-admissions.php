<?php
/*
Template Name: Admissions
*/
?>
<?php get_header(); ?>
<section class="banner admissions">
	<div class="row vert-pad">
		<div class="large-6 columns text-center vert-pad-large">
			<h1>Admissions</h1>
			<p>Calling a help line is a big step on the road to recovery. It’s important to know that you will be speaking to someone with a vested interest in your healing. Our admissions coordinators have been where you are now and have had their lives affected one way or another with addiction. From the moment you choose The Oaks, we help you through the admissions process.</p>			
		</div>
		<div class="large-6 columns">
			<div class="video-box horz-marg-small border">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/pyaaZZ_V1VY?rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">					
					<p class="text-center">What to expect when you call</p>
				</div>				
			</div>
		</div>
	</div>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-9 large-centered columns">
			<h2 class="text-center italic">All Calls Are 100% Confidential</h2>
		</div>
	</div>
</section>

<section role="main">
	<section class="option-boxes">
		<article class="row">
			<div class="large-3 columns">
				<h2 class="underlined">Initial Call
				</h2>
			</div>
			<div class="large-9 columns">
				<div class="box">
					<p>Asking for help is an important starting point of your recovery. To help ease any concerns about the initial phone call, our coordinators will guide you through a free, confidential assessment. Your phone calls are never recorded, and always remain 100% confidential. After reviewing your assessment and your insurance information, our experienced staff will determine the most appropriate level of care for you or your loved one.</p>
					<h3 class="text-center">Questions you might expect</h3>
					<ul class="large-block-grid-2 small-block-grid-1">
						<li>What current issues prompted the phone call?</li>
						<li>Does the individual have any previous treatment history?</li>
						<li>Does the individual have a co-occurring disorder that requires treatment at a residential level of care or a partial hospitalization level of care?</li>
						<li>Does the individual have an insurance policy to cover treatment in the Foundations system?</li>
					</ul>
				</div>
			</div>
		</article>
		<article class="row">
			<div class="large-3 columns">
				<h2 class="underlined">Scheduling
					Admission
				</h2>
			</div>
			<div class="large-9 columns">
				<div class="box">
					<p>Once the admissions coordinator has gathered the appropriate information, the next step is to schedule the client for admission as soon as possible. The admission date and time is offered seven days a week.</p>
					<div class="row">					
						<div class="large-8 columns">						
							<h3 class="text-center">Coordination of Travel</h3>
							<p>Patients travel to The Oaks from all over the country. From the time a potential client calls our helpline, we begin working with you and your family to begin your recovery process. We can assist with travel arrangements and book flights for a client or they can arrange for their own travel to our facility. While The Oaks cannot compensate for airfare costs, our travel agents will work with you to find the best pricing options for your trip. </p>
							<p>Since discharge dates fluctuate based on the completion of a client’s personal program, the staff can help make return flight arrangements nearer to departure time.</p>
						</div>
						<div class="large-4 columns top-marg-xsmall">
							<div class="flex-video">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1630.0013930434377!2d-89.77165854269033!3d35.20639956347214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x887f9e06f0068669%3A0xcbd9f5f52d283da7!2sLakeside%20Behavioral%20Health%20System!5e0!3m2!1sen!2sus!4v1585671763540!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0"></iframe>
							</div>
						</div>
					</div>						
				</div>				
			</div>
		</article>
		<article class="row">
			<div class="large-3 columns">
				<h2 class="underlined">Checking in
				</h2>
			</div>
			<div class="large-9 columns">
				<div class="box">
					<p>Clients are met at the airport by a staff member who will escort them to The Oaks and begin the paperwork process. All consent forms and release information along with financial paperwork is discussed and any questions are answered. A medical assessment follows, and the client is assisted through the check-in process by a residential coordinator on hand to answer any questions about rules and regulations. The staff is also here to help with family medical leave paperwork and can communicate with a client’s personal doctor, job supervisor, lawyer or probation officer when necessary. In most cases, this is all handled in the first 24 hours of a client’s stay.</p>
					<div class="row">
						<div class="large-8 columns">
							<h3 class="text-center">Verifying your benefits</h3>
							<p>Prior to your arrival, our trained staff will verify your insurance benefits and discuss any out-of-pocket expenses you may incur depending on your coverage. Out-of-pocket expenses may vary depending on your policy, but our admissions coordinators will contact your insurance company directly to determine the appropriate level of care for you or your loved one.</p>
						</div>
						<div class="large-4 columns text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/style/images/insurance.png" alt="Insurance Logos">
						</div>
						<div class="small-12 columns text-center top-marg-xsmall">
							<a href="#" data-reveal-id="checklist" class="button outline small round">Checklist: what to bring</a>
						</div>
					</div>
				</div>				
			</div>
		</article>		  			
		<div id="checklist" class="reveal-modal" data-reveal>
		  <h2 class="text-center">Checklist: what to bring</h2>
		  <div class="medium-6 columns">
		  	<h3 class="text-center">Approved/Recommended Items</h3>
		  	<ul class="checkmark">
		  		<li>Everyday Clothes. Clothes that are an appropriate seasonal attire. For winter,”Long Johns”, gloves, hats, scarves.</li>
		  		<li>Personal Hygiene Items</li>
		  		<li>Approved Prescription Drugs – minimum of a 2-week supply of prescribed medications must be in appropriately labeled bottles (your name, medication name, dosage, frequency.) Fill any prescriptions prior to admission. Over-the-counter medications must be approved by a physician and will be dispensed and maintained by appropriate staff.</li>
		  		<li>Picture Identification</li>
		  		<li>Pharmacy Benefit Card – if no Pharmacy Benefit Card, sufficient money/credit card to cover medication costs.</li>
		  	</ul>
		  </div>
		  <div class="medium-6 columns">
		  	<h3 class="text-center">Prohibited Items</h3>
		  	<ul class="xmark">
		  		<li>“Short” shorts, tops that expose the midriff in any position, tops with straps, short shorts or skirts, revealing clothing, clothing with alcohol, derogatory, sexual or drug logos, clothing that promotes gang themes.</li>
		  		<li>Weapons/pocket knives</li>
		  		<li>Alcohol or drugs</li>
		  		<li>Aerosol cans</li>
		  		<li>Perfumes or toiletries containing alcohol Vitamins and health food supplements</li>
		  		<li>Automobile</li>
		  		<li>Pagers, Computers, Boom boxes, DVD’s, PDA’s, iPods, iTouch, Walkmans, video games, cameras, televisions</li>
		  		<li>All foods, including energy bars and health food supplements</li>
		  	</ul>
		  </div>				  
		  <a class="close-reveal-modal">&#215;</a>
		</div>
	</section>
</section>

<?php get_footer(); ?>