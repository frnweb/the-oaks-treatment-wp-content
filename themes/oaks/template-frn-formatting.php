<?php
/*
Template Name: FRN Formatting
*/
?>
<?php get_header(); ?>
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center"><?php the_title(); ?></h1>
			<?php get_template_part('library/includes/breadcrumbs'); ?>		
		</div>
	</div>	
</header>
<div class="row">
<div class="large-12 columns">
<div id="content">
<?php 
// This is where all of the sample formatting stuff goes
// Please note the code in the <textarea> requires no formatting/spacing tabs.
?>

<style>code{background: none; border: none;}textarea{overflow: hidden;}</style>
<br>

<article>
<div class="row">
	<div class="large-6 columns">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 200px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">

<div class="sl_divider divider"></div>

	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 30px;">
[divider]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->
</div><!-- /.row -->


<div class="row">
	<div class="small-12 columns">
			<code class="clearfix">
			<textarea style="height: 500px;">
[row]
[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]
[/row]
		</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
		<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 250px;">
<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.”
<cite>Noah benShea</cite></blockquote>
<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Un-ordered Lists</h4>
		<ul>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="secondary">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="disc">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 550px;">
<h4>Un-ordered Lists</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="secondary">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="disc">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Ordered Lists</h4>
		<ol>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ol>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Ordered Lists</h4>
<ol>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ol>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box-large">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 700px;">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box-large"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<div class="sl_callout callout">
			<h4>H4. This is the Default Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout secondary">
			<h4>H4. This is the Secondary Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout dark">
			<h4>H4. This is the Dark Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout border">
			<h4>H4. This is a Border Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout-card callout-card">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/theoaks-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is the Default Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card secondary">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/theoaks-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Secondary Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card border">
			<div class="card-media" style="background-image: url(<?php bloginfo('template_directory'); ?>/style/images/theoaks-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Border Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 1000px;">
[callout]
<h4>H4. This is the Default Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="secondary"]
<h4>H4. This is the Secondary Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="dark"]
<h4>H4. This is the Dark Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="border"]
<h4>H4. This is a Border Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout-card img="/wp-content/themes/oaks/style/images/theoaks-facility.jpg"]
<h4>This is the Default Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/oaks/style/images/theoaks-facility.jpg" style="secondary"]
<h4>This is a Secondary Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/themes/oaks/style/images/theoaks-facility.jpg" style="border"]
<h4>This is a Border Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-5 columns">
		<p><a href="#" class="sl_button button " target="_self">Button</a> <a href="#" class="sl_button button secondary" target="_self">Button</a> <a href="#" class="sl_button button dark" target="_self">Button</a> <a href="#" class="sl_button button border" target="_self">Button</a></p>
		<div class="sl_read-next--buton">
			<h4>Read This Next:</h4>
			<p><a href="#" class="sl_button button read-next" target="_self">Long Article Title</a></p>
		</div>
	</div><!-- /.col columns -->
	<div class="large-7 columns">
			<textarea style="height: 200px;">
[button url="#"]Button[/button]
[button url="#" style="secondary"]Button[/button]
[button url="#" style="dark"]Button[/button]
[button url="#" style="border"]Button[/button]
[button url="#" style="read-next"]Long Article Title[/button]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="large-6 sl_columns columns">
		<?php echo do_shortcode('[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
		<?php echo do_shortcode('[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
	</div><!-- /.col columns -->

	<div class="large-6 sl_columns columns">
			<textarea style="height: 100px;">
[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]
[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
<dl class="sl_tabs tabs" data-tab>
	<dd class="active"><a href="#panel2-1">Tab One Title</a></dd>
	<dd><a href="#panel2-2">Tab Two Title</a></dd>
	<dd><a href="#panel2-3">Tab Three Title</a></dd>
</dl>
<div class="sl_tabs-content tabs-content">
	<div class="sl_content content active" id="panel2-1">
	<h4>Tab 1 Content</h4>
	<p>This content is exclusive to tab one. Place your tab one content here.</p>
	</div>
	<div class="sl_content content" id="panel2-2">
	<h4>Tab 2 Content</h4>
	<p>This content is exclusive to tab two. Place your tab one content here.</p>
	</div>
	<div class="sl_content content" id="panel2-3">
	<h4>Tab 3 Content</h4>
	<p>This content is exclusive to tab three. Place your tab one content here.</p>
	</div>
</div><!--/.tabs-content-->
<dl class="sl_tabs tabs secondary" data-tab>
	<dd class="active"><a href="#panel3-1">Tab One Title</a></dd>
	<dd><a href="#panel3-2">Tab Two Title</a></dd>
	<dd><a href="#panel3-3">Tab Three Title</a></dd>
</dl>
<div class="sl_tabs-content tabs-content secondary">
	<div class="sl_content content active" id="panel3-1">
	<h4>Tab 1 Content</h4>
	<p>This content is exclusive to tab one. Place your tab one content here.</p>
	</div>
	<div class="sl_content content" id="panel3-2">
	<h4>Tab 2 Content</h4>
	<p>This content is exclusive to tab two. Place your tab one content here.</p>
	</div>
	<div class="sl_content content" id="panel3-3">
	<h4>Tab 3 Content</h4>
	<p>This content is exclusive to tab three. Place your tab one content here.</p>
	</div>
</div><!--/.tabs-content-->
	</div><!-- /.col columns -->
	<div class="large-6 columns">
			<textarea style="height: 570px; overflow-y: scroll;">
[tabs]
[tab-title]Tab One Title[/tab-title]
[tab-title]Tab Two Title [/tab-title]
[tab-title]Tab Three Title [/tab-title]
[/tabs]

[tab-content]
[tab-panel]
<h4>Tab 1 Content</h4>
This content is exclusive to tab one. Place your tab one content here.

[/tab-panel]

[tab-panel]
<h4>Tab 2 Content</h4>
This content is exclusive to tab two. Place your tab two content here.

[/tab-panel]

[tab-panel]
<h4>Tab 3 Content</h4>
This content is exclusive to tab three. Place your tab three content here.

[/tab-panel]
[/tab-content]

[tabs style="secondary"]
[tab-title]Tab One Title[/tab-title]
[tab-title]Tab Two Title [/tab-title]
[tab-title]Tab Three Title [/tab-title]
[/tabs]

[tab-content style="secondary"]
[tab-panel]
<h4>Tab 1 Content</h4>
This content is exclusive to tab one. Place your tab one content here.

[/tab-panel]

[tab-panel]
<h4>Tab 2 Content</h4>
This content is exclusive to tab two. Place your tab two content here.

[/tab-panel]

[tab-panel]
<h4>Tab 3 Content</h4>
This content is exclusive to tab three. Place your tab three content here.

[/tab-panel]
[/tab-content]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">	
	<div class="large-6 columns">
<dl class="sl_accordion accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<dd><a href="#panel1">Accordian Title 1</a>
<div id="panel1" class="sl_content content">This is my accordion content 1.</div>
</dd>
<dd><a href="#panel2">Accordian Title 2</a>
<div id="panel2" class="sl_content content">This is my accordion content 2.</div>
</dd>
<dd><a href="#panel3">Accordian Title 3</a>
<div id="panel3" class="sl_content content">This is my accordion content 3.</div>
</dd>
</dl>

<dl class="sl_accordion accordion secondary" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<dd><a href="#panel4">Accordian Title 1</a>
<div id="panel4" class="sl_content content">This is my accordion content 1.</div>
</dd>
<dd><a href="#panel5">Accordian Title 2</a>
<div id="panel5" class="sl_content content">This is my accordion content 2.</div>
</dd>
<dd><a href="#panel6">Accordian Title 3</a>
<div id="panel6" class="sl_content content">This is my accordion content 3.</div>
</dd>
</dl>
	</div><!-- /.col columns -->


	<div class="large-6 columns">
			<textarea style="height: 190px;">
[accordion]
[accordion-item title="Accordian Title 1"]This is My accordion content 1.[/accordion-item]
[accordion-item title="Accordian Title 2"]This is my accordion content 2.[/accordion-item]
[accordion-item title="Accordian Title 3"]This is my accordion content 3.[/accordion-item]
[/accordion]

[accordion style="secondary"]
[accordion-item title="Accordian Title 1"]This is My accordion content 1.[/accordion-item]
[accordion-item title="Accordian Title 2"]This is my accordion content 2.[/accordion-item]
[accordion-item title="Accordian Title 3"]This is my accordion content 3.[/accordion-item]
[/accordion]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="large-8 sl_columns columns">
		<?php echo do_shortcode('[email not_content="true"]'); ?>
		<?php echo do_shortcode('[email heading="You can customize this" campaign="specific_campaign" not_content="true"]'); ?>
	</div><!-- /.col columns -->

	<div class="large-4 sl_columns columns">
			<textarea style="height: 300px;">
[email]
[email heading="You can customize this" campaign="specific_campaign"]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="large-8 sl_columns columns">
		<?php echo do_shortcode('[cta]'); ?>
		<?php echo do_shortcode('[cta style="inverse"]'); ?>
	</div><!-- /.col columns -->

	<div class="large-4 sl_columns columns">
			<textarea style="height: 300px;">
[cta]
[cta style="inverse"]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->
</article>

</div><!-- end content -->
</div><!-- end wrapper div -->
</div><!-- end inside div -->

<?php get_footer(); ?>