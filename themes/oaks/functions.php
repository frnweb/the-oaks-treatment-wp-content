<?php
//Setup
require_once (TEMPLATEPATH . '/library/functions/setup.php');

//Frothy Functions
require_once (TEMPLATEPATH . '/library/functions/ranklab-functions.php');

//Frothy Features
require_once (TEMPLATEPATH . '/library/functions/ranklab-features.php');

//Frothy Comments
require_once (TEMPLATEPATH . '/library/functions/ranklab-comments.php');

//Sidebars
require_once (TEMPLATEPATH . '/library/functions/sidebars.php');

//Custom Widgets
require_once (TEMPLATEPATH . '/library/functions/widgets.php');

//Taxonomies
require_once (TEMPLATEPATH . '/library/functions/taxonomies.php');

//Post Types
require_once (TEMPLATEPATH . '/library/functions/posttypes.php');

//Meta Boxes
require_once (TEMPLATEPATH . '/library/functions/meta-boxes.php');

//Shortcodes
require_once (TEMPLATEPATH . '/library/functions/shortcodes.php');

//////////////////////////////
// SOCIAL IMAGE OPENGRAPH   //
//////////////////////////////

if(!function_exists('frn_opengraph_images')) {
function frn_opengraph_images () {
    $options_social=get_option('frn_social_options');
    //if(!isset($options_social['opengraph_trigger'])) $options_social['opengraph_trigger']="";
    //if($options_social['opengraph_trigger']=="Activate") {
	    $metas="";
	    if(is_single() || is_page()) {
	        $images = get_attached_media( 'image' );
	        
	        if(is_array($images)) {
	            foreach($images as $image) {
	                if($image->guid!=="") $metas .= '     <meta property="og:image" content="'.$image->guid.'" />'."\n";
	            }
	        }
	    }
	    elseif(is_category()) {
	        $category = get_the_category(); 
	        //Gets a list of all posts in the category
	        $args = array( 
	            'post_type' => 'post', 
	            'cat' => $category[0]->cat_ID, 
	        );
	        $all_posts = new WP_Query($args); 
	        if ( $all_posts->have_posts() ) {
	            while ( $all_posts->have_posts() ) {
	                $all_posts->the_post();
	                //try easy method just looking for featured image
	                if(has_post_thumbnail()) {
	                    $metas .= '     <meta property="og:image" content="'.get_the_post_thumbnail_url( null, 'full' ).'" />'."\n";
	                }
	                //If it didn't find one, then pull from the content, which requires a new query
	                else {
	                    $args = array(
	                        'post_type'      => 'attachment',
	                        'post_parent'    => get_the_id(),
	                        'post_mime_type' => 'image',
	                        'post_status'    => null,
	                        'numberposts'    => -1,
	                    );
	                    $attachments = get_posts($args);
	                    if ($attachments) {
	                        foreach ($attachments as $attachment) {
	                            $metas .= '     <meta property="og:image" content="'.wp_get_attachment_url($attachment->ID).'" />'."\n";
	                        }
	                        
	                    }
	                }
	            }
	            
	        } else {
	            // no posts found
	        }
	        wp_reset_postdata();
	    }
	    else {
	        $images = get_attached_media( 'image' );
	        if(is_array($images)) {
	            foreach($images as $image) {
	                if($image->guid!=="") $metas .= '     <meta property="og:image" content="'.$image->guid.'" />'."\n";
	            }
	        }
	    }

	    //The following applies for all pages on the site.
	    //By default, we always add the default image as an option in case people prefer it, but it needs to be last so it's last in the Facebook image options.
	    if(isset($options_social['default_img'])) {
            if(trim($options_social['default_img'])!=="") $metas.='     <meta property="og:image" content="'.$options_social['default_img'].'" />'."\n";
        }

	    if($metas!=="") {
	        echo " \n \n <!-- FRN Custom Opengraph --> \n ";
	        echo $metas;
	        echo " <!-- END FRN Custom Opengraph --> \n \n ";
	    }
	//} //end trigger
}
add_action('wp_head', 'frn_opengraph_images');
}

?>