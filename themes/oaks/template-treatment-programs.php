<?php
/*
Template Name: Treatment Programs
*/
?>
<?php get_header(); ?>
<section class="banner treatment-programs">
	<div class="row vert-pad">
		<div class="large-6 columns text-center vert-pad-large">
			<h1>Treatment Programs</h1>
			<p>Our treatment model is designed to work with individuals in all stages of the recovery process. Each path to overcoming addiction is unique, and our experienced team works with you or your loved one through every step of the journey.</p> 			
		</div>
		<div class="large-6 columns">
			<div class="video-box horz-marg-small border">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7IE-KQYtk5c?rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">					
					<p class="text-center">The Music Program at The Oaks</p>
				</div>				
			</div>
		</div>
	</div>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-9 large-centered columns">
			<h2 class="text-center italic">At The Oaks, we are committed to providing integrated, evidence-based treatment for anyone battling with co-occurring mental health and substance abuse disorders.</h2>
		</div>
	</div>
</section>

<section>
<article class="row" style="margin-bottom:2.5rem;">
<div class="box">
<div class="row">  
  <div class="large-4 columns">
<h2 class="underlined">Treatment and Therapy</h2>
<p class="text-center">Using group as well as individual therapy, we work with you to identify and focus on specific co-occurring disorders and treat them with appropriate, evidence-based treatments. This therapy incorporates cognitive, behavioral and dynamic elements to provide you with new information and perspectives on addiction, new coping strategies for dealing with external and internal triggers, and practice in applying those tools.</p>
</div>
<div class="large-8 columns">
<div class="panel callout radius">
<h3 class="subheader">Here’s an overview of some of the therapeutic methods we practice at The Oaks:</h4>
<ul>
	<li><a href="<?php echo get_site_url(); ?>/dialectical-behavior-therapy/">Dialectical Behavior Therapy skills</a></li>
	<li><a href="<?php echo get_site_url(); ?>/addiction-therapy/cognitive-behavioral-therapy/">Cognitive Behavioral Therapy</a></li>
	<li><a href="<?php echo get_site_url(); ?>/programs/trauma-resolution/">Cognitive Processing Therapy</a></li>
	<li>Acceptance and Commitment Therapy</li>
	<li><a href="<?php echo get_site_url(); ?>/addiction-therapy/eye-movement-desensitization-reprocessing/">Eye Movement Desensitization and Reprocessing (EMDR)</a></li>
	<li>Diverse educational and process groups, including trauma resolution, healthy boundaries, codependency, spirituality, anger management and family systems</li>
	<li><a href="<?php echo get_site_url(); ?>/programs/intensive-family/">Family therapy</a>, including our Intensive Family Weekend with educational lectures and process groups</li>
	<li>Holistic therapy options, including meditation, acupuncture, massage and mindfulness practices</li>
</ul>
</div>
</div>
</div>
</article>
</section>

<section role="main" class="row">
		<section class="large-9 columns option-boxes">
		<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Music Therapy
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
		<p>Our innovative music program emphasizes healing through creative expression. Proficiency is not required; patients can make music with all kinds of instruments, and our music therapist will help them process their experiences and the work of treatment through music. With <a href="<?php echo get_site_url(); ?>/about/music-room/">our Blue Door Recording Studio</a>, patients can even record their own songs and take the recording home with them after they leave treatment.</p>
        <div class="small-text-center medium-text-right">
							<a href="<?php echo get_site_url(); ?>/about/music-room/" class="button small round">Learn More</a>	
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Intensive
						Family Program
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<img class="left right-marg-xsmall bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/intensive-family-program.jpg" alt="Intensive Family Program">
						<p>We recognize the importance and need for family involvement (whenever possible) in the recovery process, because family support can be key to an individual's long-term success. At The Oaks, we provide education about the impact of addictive behavior and mental health issues on the family as well as the challenges that arise for the family when a patient begins the recovery process. We work toward making family therapy a significant part of each individual's treatment experience. We believe that in order to provide the best treatment possible for each patient, we need to make decisions about family therapy on a case-by-case basis following discussion with treatment teams and each individual. We believe that The Oaks provides many opportunities for patients to work on family issues in group and individual therapy as well as our Intensive Family Program, which takes place over a weekend. During that weekend, our sessions and groups provide an examination of the characteristics, histories, experiences and dreams of our families with a particular eye toward each individual’s role and responsibility in the family system.</p>				
						<div class="small-text-center medium-text-right">
							<a href="<?php echo get_site_url(); ?>/programs/intensive-family" class="button small round">Learn More</a>
						</div>	
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Trauma Recovery
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<p>Trauma can affect anyone. At The Oaks, we know the importance of resolving the effects of trauma, which often contribute to substance use and mental health issues. We utilize a number of effective modalties to help patients process pain, negative thoughts, depression and anxiety. Healing from trauma paves the path to long-lasting recovery. We use three therapies specifically in regard to trauma issues: Eye Movement Desensitization and Reprocessing (EMDR), Acceptance and Commitment Therapy (ACT) and Cognitive Processing Therapy (CPT). EMDR disarms past experiences that have set the groundwork for patients’ thinking and feeling patterns. It resets the path of dysfunctional emotions, beliefs and sensations while instilling a positive experience that is needed to enhance future adaptive behaviors and mental health. ACT helps patients take control of their lives and determine how much importance negative thought patterns and habits will receive. And CPT includes written or conversational exercises where the patient learns to take into account the context of the event, why it happened, his or her real role in the event and what is stopping the emotional processing from achieving natural closure. At The Oaks, we use these approaches and others to help each patient discover his or her most precious values and act on them. By learning to notice and accept unpleasant thoughts or feelings without giving them the power to control behaviors and attitudes, a person can once again enjoy a meaningful life.</p>				
						<div class="small-text-center medium-text-right">
							<a href="<?php echo get_site_url(); ?>/programs/trauma-resolution" class="button small round">Learn More</a>
						</div>	
					</div>
				</div>
			</article>
<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Creative Arts
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<p>At The Oaks, we believe that creative arts can be a unique and powerful part of the overall healing experience. In the context of substance abuse treatment, these exercises can increase a patient’s creativity through art interactions, helping to build self-esteem and enhance the individual’s strengths while exploring issues that may not be as easy to address in traditional therapy sessions. Methods may include collage, painting, drawing and other forms of art, each of which can help teach different lessons and enhance different areas of creativity. Since so much of treatment is verbal—discussing issues and talking through problems and barriers to sobriety—a non-verbal option like creative arts can provide a welcomed break and allow patients to explore themselves and their addictions in a new way, often leading to fresh insights and a new perspective.</p>				
				</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Exercise
					and Recreation
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<p>In 2015, the Oaks underwent some new developments in recreation, including the addition of a scenic nature trail, a sport court for basketball and volleyball, shuffleboard, and <a href="<?php echo get_site_url(); ?>/programs/exercise-recreation/">new equipment and curriculum for the in-house gym</a>. We believe that engaging in exercise and activity is healthy for mind, body and spirit healing, and it can help patients develop new, positive habits as they pursue a life in recovery.</p>				
				</div>
				</div>
			</article>
            <article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Facility Features
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<p>In addition to our nature trail, <a href="<?php echo get_site_url(); ?>/about/nature-outdoor-features/">the landscaping at The Oaks</a> is designed to emphasize our natural environment with an assortment of seasonal flower beds and greenery. We have bird feeders, a fountain and a fire pit for patients to enjoy, and our emphasis on music and the arts led to the addition of a large, <a href="<?php echo get_site_url(); ?>/about/inspirational-art/">colorful mural on one of our buildings.</a> </p>				
				</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Life Challenge Program</h2>	
				</div>
				<div class="large-9 columns">
					<div class="box">
					  <p>You faced the challenge of treatment head on, establishing healthy habits and using new tools to get through the tough days. You&rsquo;ve done the hard work and set new goals. Now, as you prepare to leave The Oaks, the real adventure begins. You face that challenge known as life. Fortunately, our <strong>Life Challenge </strong>program (aka the L+C) has your back. </p>					
						<div class="small-text-center medium-text-right">
							<a href="<?php echo get_site_url(); ?>/about/alumni" class="button small round">Learn More</a>
						</div>
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Case Management
						and Aftercare
					</h2>
				</div>
				<div class="large-9 columns">	
					<div class="box">
						<p>Treatment is the beginning of a lifelong journey of discovery and recovery, but what happens after treatment is equally as important as the treatment itself. We at La Paloma make every effort to assure that each client has a workable continuing care plan and the structure to sustain the growth generated in treatment.</p>					
						<div class="small-text-center medium-text-right">
							<a href="<?php echo get_site_url(); ?>/programs/case-management-aftercare" class="button small round">Learn More</a>
						</div>
					</div>
				</div>
			</article>
		</section>
		<aside class="large-3 columns">
			<div class="grey-cta trees">		
				<a href="<?php echo get_site_url(); ?>/insurance">
					<h3>Learn how insurance can help pay for treatment</h3>
				</a>	
			</div>
			<div class="telephone-cta vert-marg-small hide-for-small">
				<p>Get Started Today</p>
				<span class="number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sidebar"]'); ?></span>
				<div class="text-center">
					<a href="<?php echo get_site_url(); ?>/contact" class="button outline small round vert-marg-tiny">Contact Us</a>
				</div>
			</div>			
		</aside>	
</section>

<?php get_footer(); ?>