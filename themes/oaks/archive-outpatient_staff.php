<?php get_header(); ?>
<section role="main">
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center">Our Outpatient Staff</h1>
			<?//php get_template_part('library/includes/breadcrumbs'); ?>	
		</div>
	</div>	
</header>
	<div class="row">		
		<?php $posts_query = new WP_Query( 'post_type=outpatient_staff&nopaging=true' );
		while ( $posts_query->have_posts() ) : $posts_query->the_post();?>
		<div class="small-12 medium-10 medium-centered columns">					
			<article class="staff vert-pad-small">								
				<div class="box">	
				<?php
				if ( has_post_thumbnail() ) {?>
					<div class="text-center">
						<div class="headshot">
							<?php the_post_thumbnail( 'medium' );?>
						</div>
					</div>						
				<?php } ?>				
				<h2 class="text-center">
					<?php the_title();?><br> 
					<small><?php echo get_post_meta($post->ID, 'staff_title', true); ?></small>
				</h2>
					<div class="top-marg-xsmall">
						<?php the_content();?>				
					</div>
				<?php if (get_post_meta($post->ID, 'staff_bio_pdf', true) ) { ?>
					<div class="text-center top-marg-xsmall">
						<a href="<?php echo get_post_meta($post->ID, "staff_bio_pdf", $single = true); ?>" class="round button small">Read more</a>
					</div>
				<?php } ?>
				</div>	
				</div>
			</article><!-- end blog -->			
		<?php endwhile; wp_reset_postdata(); ?>
		</div>	
	</div>			
</section>
<?php get_footer(); ?>  