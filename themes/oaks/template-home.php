<?php
/*
Template Name: Homepage
*/
?>
<?php get_header(); ?>
<section class="home-banner">
	<div class="row welcome">
		<div class="large-6 columns large-text-right">
			<h1>The Oaks at La Paloma<br>	 
				<small>More than a residential treatment center</small>
			</h1>			
			<a href="<?php echo get_site_url(); ?>/treatment" class="secondary round button action">Explore Treatment Options</a>
		</div>		
	</div>
	<section class="steps">
		<ul class="medium-block-grid-5 hide-for-small">
			<li><a href="<?php echo get_site_url(); ?>/about">Why choose<span class="sub-step">the Oaks?</span></a></li>
			<li><a href="<?php echo get_site_url(); ?>/treatment">What are my<span class="sub-step">treatment options?</span></a></li>
			<li><a href="<?php echo get_site_url(); ?>/treatment-center-photo-gallery">Take a tour<span class="sub-step">of the Oaks</span></a></li>
			<li><a href="<?php echo get_site_url(); ?>/admissions">How do I<span class="sub-step">get started?</span></a></li>
			<li><a href="<?php echo get_site_url(); ?>/contact">Get in touch<span class="sub-step">talk to someone now</span></a></li>
		</ul>
	</section>
</section> 
<section class="spacing">
	<div class="row">
		<div class="large-6 columns">
			<div class="video-box horz-marg-small">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/XuA_QpKZTkc" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">
					<div class="row">
						<div class="small-12 columns text-center">Accredited with: 
							<a href="https://www.jointcommission.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/style/images/jcaho-silver.png" alt="Jcaho Logo"></a>
						</div>						
					</div>
				</div>
			</div>			
		</div>
		<div class="large-6 columns text-center horz-pad-small top-marg-small hide-for-small">
			<h2>Expert Addiction Treatment</h2>
			<p>We are providers of integrated treatment to persons with addiction and/or mental health conditions. Through the use of the Foundations Treatment Model, our specially trained staff members address the complex needs of individuals struggling against co-occurring disorders.</p>
			<a href="<?php echo get_site_url(); ?>/about" class="button small round">About Us</a>
			<div class="telephone-cta top-marg-small">
				<p>Get Started Today</p>
				<span class="number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Homepage Body"]'); ?></span>
			</div>
		</div>
	</div>
</section>
<section class="spacing bluedoor-home">
	<div class="row">	
		<div class="medium-10 medium-centered columns text-center">
			<h2>Blue Door Recording Studio</h2>
			<p>When you’re in a city steeped in music history, it’s only natural to include music in your programming. Memphis is the home of Elvis Presley and Sun Studios, among others. The blues originated here, and Beale Street remains an attraction for music-minded visitors. Playing music can be very therapeutic, and our music program emphasizes healing through creative expression.</p>
			<a href="<?php echo get_site_url(); ?>/about/music-room" class="button small round">Learn More</a>
		</div>
	</div>
</section>
<section class="spacing">
	<div class="row">
		<div class="large-6 columns text-center horz-pad-small">
		<h2>Insurance Options</h2>
			<p>Our comprehensive program is covered by most health insurance plans. If you have questions about whether you or your loved one’s insurance policy covers substance abuse or mental health treatment, please give us a call us to verify your coverage.</p>
			<a href="<?php echo get_site_url(); ?>/insurance" class="button small round">Insurance Coverage</a>
		</div> 
		<div class="large-6 large-uncentered columns horz-pad-small">
			<h3>Some of the insurances we accept:</h3>
			<ul>
				<li>BlueCross BlueShield</li>
				<li>Aetna</li>
				<li>Cigna</li>
				<li>Humana</li>
				<li>MultiPlan</li>
				<li>Magellan Health</li>
				<li>Value Options</li>
			</ul>
		</div>
	</div>
</section>
<section class="spacing treatment-focus">
	<div class="row" data-equalizer>
		<div class="large-2 medium-3 columns">
			<a href="<?php echo get_site_url(); ?>/intervention">
				<div class="box-btn text-center" data-equalizer-watch>
					How 
					Intervention Works
				</div>
			</a>		
		</div>
		<div class="large-2 medium-3 columns">
			<a href="<?php echo get_site_url(); ?>/co-occurring-disorders">
				<div class="box-btn text-center" data-equalizer-watch>
					Co-Occurring 
					Disorders
				</div>
			</a>
		</div>
		<div class="large-8 medium-6 columns text-center horz-pad-small">
			<h2>Detoxification</h2>
			<p>Detoxification is considered the first step on the road to recovery from an addiction. We offer a comprehensive, medically supervised detoxification that allows those with an addiction to safely and carefully get the substances out of their system with the least amount of discomfort and risk.</p>
			<a href="<?php echo get_site_url(); ?>/drug-detox" class="button small round">Learn More</a>
		</div>
	</div>
</section>
<section class="spacing">
	<div class="row">		
		<div class="large-6 columns large-push-6 text-center horz-pad-small top-marg-small">
			<h2>The Oaks Staff</h2>
			<p>Our experienced and compassionate staff members at The Oaks are specially trained to address the complex needs of those suffering from addiction and are committed to seeing you or your loved one through the process with the highest level of professionalism and care.</p>
			<a href="<?php echo get_site_url(); ?>/about/staff" class="button small round">Meet the Team</a>
		</div>
		<div class="large-6 columns large-pull-6 horz-pad-small">
		<ul class="medium-block-grid-3 small-block-grid-2 staff">
			<?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=6&staff_location=homepage' );
			while ( $the_query->have_posts() ) : $the_query->the_post();?>
			 <li class="text-center">
			 	<div class="headshot">
			 	<?php if ( has_post_thumbnail() ) {
		    	$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' );
		     	echo '<img width="100%" height="100%" src="' . $image_src[0] . '">';
				} ?>
				</div>
			</li>
		<?php endwhile; wp_reset_postdata(); ?>	
		</ul>	
		</div>
	</div>
</section>
<section class="spacing quote">
	<div class="row">		
		<div class="medium-10 medium-centered columns text-center">
			<blockquote>
				By treating people – not disorders – we can provide comprehensive care worthy of the trust you’ve placed in us, and help alleviate the pain for everyone involved.
				<cite>Sally R Lawes, EdS, LPC-S<br>CEO, The Oaks at La Paloma Treatment Center
</cite>
			</blockquote>			
			<a href="<?php echo get_site_url(); ?>/about/ceo-message" class="button outline small round">Welcome from our CEO</a>
		</div>
	</div>
</section>
<?php get_footer(); ?>