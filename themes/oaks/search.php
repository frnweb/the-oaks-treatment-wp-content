<?php get_header(); ?>
<section role="main">	
	<header id="page-id">
		<div class="row">
			<div class="small-12 columns">
				<h1 class="text-center">Search Results</h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</div>
		</div>
	</header>
<article>
	<div class="row">
		<div class="large-8 columns">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>

				<div class="entry">

					<?php the_excerpt(); ?>

				</div>

			</div>

		<?php endwhile; ?>
	<?php else : ?>

		<h2>No posts found.</h2>

	<?php endif; ?>		
		</div>
		<aside role="complementary" class="large-4 columns">
			<form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
			<div>
    			<label for="s" class="screen-reader-text">Search for:</label>
    			<input type="text" id="s" name="s" value="" />
    
    			<input type="submit" value="Search" id="searchsubmit" class="button small round" />
			</div>
			</form>
		</aside>
		<?php get_sidebar(); ?>
	</div>	
</article>
</section>
<?php get_footer(); ?>

