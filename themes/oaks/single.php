<?php get_header(); ?>
<section role="main">
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center"><?php the_title(); ?></h1>
			<?php get_template_part('library/includes/breadcrumbs'); ?>		
		</div>
	</div>	
</header>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<div class="row">
		<div class="large-8 columns">
		<!-- post details -->
		<div class="postmeta">
			<p class="date"><?php the_time('F j, Y'); ?></p>			
			<p class="tags"><?php the_tags( __( 'Tags: ' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></p>			
			<p class="categories"><?php _e( 'Categorized in: ' ); the_category(', '); // Separated by commas ?></p>			
		</div><!-- end postmeta -->
		<!-- /post details -->
		
		<!-- post thumbnail -->		
			<?php
			if ( has_post_thumbnail() ) {
				echo '<div class="right-marg-xsmall blog-featured-img">';
				the_post_thumbnail( 'large' );
				echo '</div>';
			}
			?>		
		<!-- end post thumbnail -->
		<article>
		<?php the_content(); ?>
		</article>			
		</div>
		<aside role="complementary" class="large-4 columns">
			<form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
			<div>
    			<label for="s" class="screen-reader-text">Search for:</label>
    			<input type="text" id="s" name="s" value="" />
    
    			<input type="submit" value="Search" id="searchsubmit" class="sl_single-button small round" />
			</div>
			</form>
		</aside>
		<?php get_sidebar(); ?>
		<div class="panel disclaimer small-12 columns top-marg-xsmall">
			<p class="text-center italic">Articles posted here are primarily educational and may not directly reflect the offerings at The Oaks. For more specific information on programs at The Oaks, contact us today.</p>
		</div>
	</div>	 
</article>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?> 