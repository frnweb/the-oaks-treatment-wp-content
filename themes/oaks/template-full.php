<?php
/*
Template Name: Full
*/
?>
<?php get_header(); ?>
<section role="main">
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center"><?php the_title(); ?></h1>
			<?php get_template_part('library/includes/breadcrumbs'); ?>
		</div>
	</div>
</header>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<div class="row">
		<div class="small-12 columns">
			<?php
			// if ( has_post_thumbnail() ) {
			// 	the_post_thumbnail( 'large' );
			// }
			?>
			<?php the_content(); ?>
		</div>
		<div class="small-12 columns top-marg-xsmall">
			<?php include(TEMPLATEPATH . "/library/includes/further-reading.php");?>
		</div>
	</div>
</article>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>