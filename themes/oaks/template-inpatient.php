<?php
/*
Template Name: Inpatient
*/
?>
<?php get_header(); ?>
<section class="banner inpatient">
	<div class="row vert-pad">
		<div class="large-6 columns text-center vert-pad-large">
			<h1>Inpatient Services</h1>
			<p>We offer proven, successful and comprehensive inpatient treatment for drug addiction, alcohol addiction and the mental health disorders that generally accompany. Many treatment centers will offer programs that deal with either the addiction or the disorder, but never both simultaneously.</p>
		</div>
		<div class="large-6 columns">
			<div class="video-box horz-marg-small border">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AIOlI6eomPs?rel=0&amp;controls=0;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">					
					<p class="text-center">What you can expect at The Oaks</p>
				</div>				
			</div>
		</div>
	</div>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-9 large-centered columns">
			<h2 class="text-center italic">At The Oaks, we are committed to providing integrated, evidence-based treatment for anyone battling with co-occurring mental health and substance abuse disorders.</h2>
			<div class="text-center">
				<ul class="medium-block-grid-4 clearing-thumbs clearing-feature vert-pad-xsmall" data-clearing>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-2.jpg" alt="" class="th"></li>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-5.jpg" alt="" class="th"></li>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center91.jpg" alt="" class="th hide-for-small"></li>
					<li class="clearing-featured-img"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center81.jpg" alt="" class="th hide-for-small"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-1.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-5.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-6.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center14.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center51.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center61.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center71.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center141.jpg" alt="" class="th"></li>
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center15.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-treatment-center121.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/la-paloma-courtyard.jpg" alt="" class="th"></li>					
					<li><img src="<?php echo get_site_url(); ?>/wp-content/uploads/La-Paloma-Treatment-Center-4.jpg" alt="" class="th"></li>	
				</ul>
				<a href="<?php echo get_site_url(); ?>/treatment-center-photo-gallery/" class="button outline small round vert-marg-tiny">Photo Tour</a>
				<a href="<?php echo get_site_url(); ?>/virtual-tour/the-oaks-at-la-paloma.html" class="button outline small round vert-marg-tiny">360° Virtual Tour</a>
			</div>
		</div>	
	</div>
</section>

<section>
	<div class="row">
		<div class="large-4 columns vert-marg-tiny">
			<div class="box-sq-light">
				<h3 class="text-center">What to Expect</h3>
				<p>
					Our experienced staff will create a recommended program based on the specific needs of you or your loved one. We provide successful treatment solutions in a therapeutic and nurturing environment with the highest level of professionalism and care. 
				</p>
			</div>
		</div>
		<div class="large-4 columns vert-marg-tiny">
			<div class="box-sq-light">
				<h3 class="text-center">Your Stay</h3>
				<p>
					While living on campus in the residential program, our clients enjoy delicious and nutritious food, an exercise weight room, meditation and quiet time, daily recreational opportunities and other healthful and fun leisure activities.
				</p>
			</div>
		</div>
		<div class="large-4 columns vert-marg-tiny">
			<div class="box-sq-light">
				<h3 class="text-center">Our Success Rate</h3>
				<p>
					The Oaks is nationally recognized for integrative methods and evidence-based treatment that produces results. Our methods have been proven more effective than traditional drug and alcohol treatment methods in a five-year research study using the Foundations Treatment Model.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="top-marg">
	<div class="row">
		<div class="large-6 columns">
			<p class="sub-text">Our comprehensive residential treatment program is an inpatient model that includes therapeutic groups, ongoing pharmacological assessment and evaluation, individual therapy, wellness and activities that address mind, body and spirit. <strong>Additional aspects of our well-rounded program include a therapeutic milieu that supports growth on the physical, spiritual and mental level.</strong></p>
		</div>
		<div class="large-6 columns"> 
			<p>While in the Residential program, each client has the opportunity to be involved in:</p>
			<ul class="arrow">
				<li>Individual therapy with an expert mental health professional</li>
				<li>Group therapy and support</li>
				<li>Family therapy (including Intensive Family Weekend)</li>
				<li>Medication management</li>
				<li>Educational groups</li>
				<li>Recreation and expressive therapy</li>
				<li>12-Step recovery</li>
				<li>Relapse prevention and life skills groups</li>
				<li>Daily contribution to the community</li>
				<li>Specialty treatment for psychological trauma and other issues as needed</li>
			</ul>
		</div>
	</div>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-3 large-offset-1 columns">
			<div class="grey-cta tree vert-pad horz-pad-xsmall vert-marg-xsmall">
				<div class="telephone-cta">
					<p>Get Started Today</p>
					<span class="number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sidebar"]'); ?></span>
				</div>
			</div>
		</div>
		<div class="large-7 large-pull-1 columns">
			<h2 class="text-center">Dual Diagnosis</h2>
			<p>Dual Diagnosis refers to a condition in which a person has both a mental illness and an addiction issue. At The Oaks, not only are we committed to treating both the addiction and the mental illness so that you or your loved one can find full and total recovery, but we are also only one of very few facilities that are prepared to deal effectively with dual diagnosis.
			</p>
			<div class="text-center">				
				<a href="<?php echo get_site_url(); ?>/dual-diagnosis" class="button small round vert-marg-tiny">Learn More</a>
			</div>
		</div>	
	</div>
</section>


<div class="row vert-pad-xsmall bottom-marg-small">
	<h2 class="text-center bottom-marg-xsmall">
		The Oaks Inpatient Staff
	</h2>
	<ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-2 staff">
		<?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=6&staff_location=inpatient' );
		while ( $the_query->have_posts() ) : $the_query->the_post();?>
		 <li class="text-center">
		 	<div class="headshot">
		 	<?php if ( has_post_thumbnail() ) {
	    	$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' );
	     	echo '<img width="100%" height="100%" src="' . $image_src[0] . '">';
			} ?>
			</div>
		</li>
	<?php endwhile; wp_reset_postdata(); ?>		
	</ul>	
	<div class="text-center top-marg-xsmall">
		<a href="<?php echo get_site_url(); ?>/about/staff" class="button small round">Meet the Team</a>
	</div>
</div>
<div class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Where we're
						Located
					</h2>
				</div>
				<div class="large-6 columns">
					<div class="box">
						<div class="flex-video"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1630.0013930434377!2d-89.77165854269033!3d35.20639956347214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x887f9e06f0068669%3A0xcbd9f5f52d283da7!2sLakeside%20Behavioral%20Health%20System!5e0!3m2!1sen!2sus!4v1585671763540!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0"></iframe></div>
					</div>
				</div>
				<div class="large-3 columns vert-pad-xlarge mobile-center" itemscope itemtype="http://schema.org/Organization">
					<h4 itemprop="name">The Oaks at Lakeside</h4>
					<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<div itemprop="streetAddress">2911 Brunswick Road</div>
						<span itemprop="addressLocality">Memphis, TN</span> <span itemprop="postalCode">38133</span>
						<div itemprop="telephone"><?php echo do_shortcode('[frn_phone]'); ?></div>
						<img class="mobile-hidden" itemprop="logo" alt="The Oaks at La Paloma" src="/wp-content/uploads/TheOaks-RoundFINAL-large.png" style="width:100px;" />
					</p>
				</div>
			</div>		 
<?php get_footer(); ?>