<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title(''); ?></title>

<!-- dns prefetch -->
<link href="//www.google-analytics.com" rel="dns-prefetch">

<!-- meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<!-- icons -->
<link href="<?php echo get_template_directory_uri(); ?>/style/images/favicon.ico" rel="shortcut icon">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
<!-- css + javascript -->
<script>  WebFontConfig = {
    google: { families: [ 'Playfair+Display:400,700:latin', 'Alegreya+Sans:300,400,700,400italic:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
</script>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=1.6" />
<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.min.js"></script>
<?php wp_head(); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLZC4QN');</script>
<!-- End Google Tag Manager -->

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLZC4QN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- header -->
<div data-closable class="sl_notification-bar" id="sl_notification-bar" style="background-color:#475824;color:#ffffff; padding: 8px 24px;">
				<style type="text/css">
					.sl_notification-bar .sl_inner { width: 100%; max-width: 1080px; margin: 0 auto; font-size: 18px; text-align: center;}
					.sl_notification-bar .sl_button { display: inline-block; padding: 6px 10px; font-size: 15px; border-radius: 3px; color: #fff!important; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2); background-color: #DE9900; letter-spacing: 0; font-weight: 400; margin-left: 8px;}
					.sl_notification-bar .close-button { position: absolute; color: #8a8a8a; right: 1rem; top: 0.5rem; font-size: 2em; line-height: 1; cursor: pointer; }
				</style>
				<div class="sl_inner">
					<span>Committed to Safety: Latest information on COVID-19 Precautions</span><a href="/important-covid-19-update-from-the-oaks-at-la-paloma/" class="sl_button">Learn More</a>
				</div>
					<div id="close-btn" class="close-button" aria-label="Close alert" style="color: #fff;" type="button" data-close>
					    <span aria-hidden="true">&times;</span>
					</div>
				</div>
			</div>
<div class="sl_message-banner">
	<p>Effective April 1, The Oaks at La Paloma will relocate to the <a href=" https://lakesidebhs.com/programs/the-oaks-at-lakeside/">Lakeside Behavioral Health System</a> campus. For more information please visit <a href=" https://lakesidebhs.com/programs/the-oaks-at-lakeside/">The Oaks at Lakeside</a>.</p>
</div>
<header style="position:relative;" id="h-main" role="navigation">
	<nav id="main-nav" class="show-for-large-up" role="navigation">
		<?php wp_nav_menu( array( 'theme_location' => 'main-nav-left', 'menu_class' => 'inline-list right', 'container_class' => 'nav-container' ) ); ?>
		<?php wp_nav_menu( array( 'theme_location' => 'main-nav-right', 'menu_class' => 'inline-list left', 'container_class' => 'nav-container' ) ); ?>
	</nav>	
	<div class="left menu-drop">
		<a id="menu-icon" onClick="ga('send', 'event', 'Hamburger Menu', 'Menu Opens & Closes'); ">Menu</a>
	</div>
	<div class="right telephone hide-for-small">
		<?php echo do_shortcode('[frn_phone action="Phone Clicks in Header"]'); ?>
	</div>	
	
		<a class="logo text-center" href="<?php echo home_url(); ?>" title="">
			<img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/oaks-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/oaks-retina-logo.png, (retina)]" alt="The Oaks at La Paloma Logo">
		</a>	 	
	<aside id="sideNav" class="small-12 columns" style="display:none;" role="navigation">
		<?php
		if ( wp_is_mobile() ) {?> 
		<div class="row">
			<div class="small-12 columns">
				<h4>The Oaks at La Paloma</h4>
				<?php wp_nav_menu( array( 'theme_location' => 'mobileNav', 'menu_class' => 'no-bullet') ); ?>	
			</div>
		</div>
		<?php } else { ?>
		<div class="row">
		<div class="medium-4 columns">
			<h4>About</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sideNav-4', 'menu_class' => 'no-bullet') ); ?>		
			<h4>Treatment</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sideNav-3', 'menu_class' => 'no-bullet') ); ?>						
		</div>
		<div class="medium-4 columns">					
			<h4>Programs</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sideNav-2', 'menu_class' => 'no-bullet') ); ?>
			<h4>Locations</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sideNav-5', 'menu_class' => 'no-bullet') ); ?>						
		</div>
		<div class="medium-4 columns">		
			<h4>Resources</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sideNav-1', 'menu_class' => 'no-bullet') ); ?>			
		</div>
		<div class="small-12 columns"><p class="italic top-marg-xsmall">The Oaks at La Paloma in Memphis, TN. provides integrated treatment for individuals with substance abuse issues and/or co-occurring mental health disorders. Our seasoned staff of therapists, counselors and coordinators use the Foundations Treatment Model to address the complex needs of those struggling with a dual diagnosis.</p></div>
		</div>
		<ul class="social inline-list right">
			<li id="facebook"><a href="https://www.facebook.com/lapalomatreatmentcenter" target="blank"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/fb.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/fb-retina.png, (retina)]" alt="Facebook"></a></li>
			<li id="twitter"><a href="https://twitter.com/La_Paloma" target="blank"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/twt.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/twt-retina.png, (retina)]" alt="Twitter"></a></li>
			<li id="linkedin"><a href="https://www.linkedin.com/company/la-paloma-treatment-center" target="blank"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (retina)]" alt="Linkedin"></a></li>
			<li id="youtube"><a href="https://www.youtube.com/channel/UC2iEx36aTAm3wVrd4mFsR-A" target="blank"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/youtube.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/youtube-retina.png, (retina)]" alt="Youtube"></a></li>
		</ul>
		<?php } ?>
	</aside>
</header>
<!-- end header-container -->