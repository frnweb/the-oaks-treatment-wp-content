jQuery(document).ready(function($) {
	//Alternating Rows
	$("#subpage .sub_item:even").addClass("alt");
	
	//Content Formatting
	//Accordion
	$('div.accordion> div').hide();  
	  $('div.accordion> h3').click(function() {
	    $(this).next('div').slideToggle('fast')
	    .siblings('div:visible').slideUp('fast');
	    $('div.accordion> div').addClass('accordion-bg');
	});
	
	//Tabs
	$('.tab-menu li a').click(function(event) {
	    event.preventDefault();
	    $('.dynamic').hide();
	    $('.tab-menu li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});
	
	//Equal Columns
	var max_height = 0;
	$("div.third").each(function(){
	    if ($(this).height() > max_height) { max_height = $(this).height(); }
	});
	$("div.third").height(max_height);
	
	//Responsive Menu
	// $('#responsive-menu-button').sidr({
	// 	name: 'sidr-main',
	// 	source: '#nav'
	// });

// });

//Reveal subnav
	$('#menu-icon').click(function() {
		showHideMenu();	
	});

	function showHideMenu() {
		$('#sideNav').slideToggle(150);
		$('#menu-icon').toggleClass('x');		
	}
	function hideMenu() {
		$('#sideNav').slideToggle(150);
		$('#menu-icon').removeClass('x');
	}

	//Adds responsive embed to all iframes from youtube
	var vidDefer = document.getElementsByTagName('iframe');
	// Remove empty P tags created by WP inside of Accordion and Orbit
	$('.accordion p:empty, .orbit p:empty').remove();
	
	$("p").find("iframe").unwrap();
	
	for (var i=0; i<vidDefer.length; i++) {
	if(vidDefer[i].getAttribute('data-src')) {
	vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
	}
	}
	
	if(!$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').parents("div").hasClass("sl_reveal")) {
	$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
	if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
	$(this).wrap("<div class='widescreen flex-video'/>");
	} else {
	$(this).wrap("<div class='flex-video'/>");
	}
	})
	}
});

//Mailchimp Email Form scripts. The markup can be found in /parts/contetn-emailform.php
jQuery(document).ready(function($) {
    var expanded = false;

    //Checks if dropdown is open and displays or hides accordingly
    function showCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

      console.log("show", checkboxes);
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    };

    //Just hides the dropdown if it's open
    function hideCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

        checkboxes.style.display = "none";
        expanded = false;
    };

    $(".sl_form__multiselect__field").focus(function(e) {
      var emailNumber = e.target.id.split("--").pop().toString();
      var optionDropdown = document.getElementById("sl_form__multiselect__field--" + emailNumber);
      var emailField = document.getElementById("mce-EMAIL--" + emailNumber);

      showCheckboxes(emailNumber);

      if (document.activeElement === optionDropdown){
        $(optionDropdown).blur();
      }

      $(emailField).on('focus', function() {
       hideCheckboxes(emailNumber);
      })

      var selectOptions = '#sl_form__multiselect__options--' + emailNumber + ' input:checkbox'

      console.log("logs", selectOptions);

    $(selectOptions).on("change", function(){
      var whoOptions = [];
      $(selectOptions).each(function(){
       if($(this).is(":checked")){
         whoOptions.push(this.parentNode.textContent)
         document.getElementById("sl_who-input--" + emailNumber).innerHTML = whoOptions.concat();
         document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If more than one option is selected
       if(whoOptions.length > 1){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "Multiple Selected";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If less than one option is selected, reformat to initial state
       if(whoOptions.length < 1 || whoOptions == undefined){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "I am a...";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "inherit";
       }
      })   
    });
    });
});
//END MAILCHIMP JS

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));