<?php
//Staff
function post_type_staff() {
    $labels = array(
        'name' => __( 'Staff' ),
        'singular_name' => __( 'Staff' ),
        'add_new' => __( 'Add New Staff' ),
        'add_new_item' => __( 'Add New Staff' ),
        'edit_item' => __( 'Edit Staff' ),
        'new_item' => __( 'Add New Staff' ),
        'view_item' => __( 'View Staff' ),
        'search_items' => __( 'Search Staff' ),
        'not_found' => __( 'No Staff found' ),
        'not_found_in_trash' => __( 'No Staff found in trash' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ), 
        'capability_type' => 'post',
        'rewrite' => array('with_front' => false, "slug" => "about/staff"), // Permalinks format
        'menu_position' => 5,
        //'menu_icon' => plugin_dir_url( __FILE__ ) . '/images/calendar-icon.gif',  // Icon Path
        'has_archive' => true
    );
    register_post_type( 'staff', $args );
}
add_action( 'init', 'post_type_staff' );

//Oupatient Staff
function post_type_outpatient_staff() {
    $labels = array(
        'name' => __( 'Outpatient Staff' ),
        'singular_name' => __( 'Outpatient Staff' ),
        'add_new' => __( 'Add New Outpatient Staff' ),
        'add_new_item' => __( 'Add New Outpatient Staff' ),
        'edit_item' => __( 'Edit Outpatient Staff' ),
        'new_item' => __( 'Add New Outpatient Staff' ),
        'view_item' => __( 'View Outpatient Staff' ),
        'search_items' => __( 'Search Outpatient Staff' ),
        'not_found' => __( 'No Outpatient Staff found' ),
        'not_found_in_trash' => __( 'No Outpatient Staff found in trash' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
        'capability_type' => 'post',
        'rewrite' => array('with_front' => false, "slug" => "outpatient-services/outpatient-staff"), // Permalinks format
        'menu_position' => 5,
        //'menu_icon' => plugin_dir_url( __FILE__ ) . '/images/calendar-icon.gif',  // Icon Path
        'has_archive' => true
    );
    register_post_type( 'outpatient_staff', $args );
}
add_action( 'init', 'post_type_outpatient_staff' );

///this fixes the custom fields not showing up bc of ACF 
add_filter('acf/settings/remove_wp_meta_box', '__return_false');
?>