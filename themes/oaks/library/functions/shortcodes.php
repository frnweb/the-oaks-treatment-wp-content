<?php



// Shortcodes based on Foundations 6.0

	// ROW
		function frn_foundation_row ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> 'small-collapse large-uncollapse medium-uncollapse'
				), $atts );
			return '<div class="sl_row row expanded content-shortcode-row ' . esc_attr($specs['style'] ). ' "> ' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('row', 'frn_foundation_row' );
	///ROW

	// COLUMN
		function frn_foundation_col ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'small'		=> '12',
				'medium'	=> '12',
				'large'		=> '12',
				), $atts );
			return '<div class="small-' . esc_attr($specs['small']) . ' medium-' . esc_attr($specs['medium']) . ' large-' . esc_attr($specs['large']) . ' sl_columns columns">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('col', 'frn_foundation_col' );
	///COLUMN

	// BUTTON
		function frn_foundation_buttons ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="sl_read-next--button"><h4>Read This Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="sl_button button secondary ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="sl_read-next--link sl_button button' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'frn_foundation_buttons' );
	///BUTTON

	//READ NEXT
	function frn_read_next ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'url'	=> '#',
			'title' => '',
			'style'	=> '', 
			'target'	=> '_self'
			), $atts );

			return '<div class="sl_read-next ' . esc_attr($specs['style'] ) . ' "><span>&gt;&gt;&gt; Read This Next:</span> <a class="sl_read-next--link" title="' . esc_attr($specs['title'] ) . '" href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
	}

	add_shortcode ('read-next', 'frn_read_next' );
	//READ NEXT

	// FLEXVIDEO
		function frn_foundation_flexvideo ( $atts, $content = null ) {
			return '<div class="flex-video">' . $content . '</div>';
		}
		add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );
	///FLEXVIDEO

	// TABS
		function frn_foundation_tabs( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			
			return '<dl class="sl_tabs tabs ' . esc_attr($specs['style'] ) . '" data-tab>' . do_shortcode ( $content ) . '</dl>';
		}

		add_shortcode ('tabs', 'frn_foundation_tabs' );
	///TABS

	// TAB TITLE
		function frn_foundation_tabs_title ( $atts, $content = null ) {

		    $specs = shortcode_atts( array(
		        'class'     => ''
		    ), $atts );

		    static $i = 0;
		    if ( $i == 0 ) {
		        $class = 'active';
		    } else {
		        $class = NULL;
		    }
		    $i++;
		    $value = '<dd class="' . $class . ' '.esc_attr($specs['class'] ).'"><a href="#panel2-' . $i . '">' .
		    $content . '</a></dd>';

		    return $value;
		}

		add_shortcode ('tab-title', 'frn_foundation_tabs_title' );
	///TAB TITLE

	// TAB CONTENT
		function frn_foundation_tabs_content( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			return '<div class="sl_tabs-content tabs-content ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('tab-content', 'frn_foundation_tabs_content' );
	///TAB CONTENT

	// TAB PANEL
		function frn_foundation_tabs_panel ($atts, $content = null ) {
			static $i = 0;
			if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}
			$i++;
			return '<div class="sl_content content ' . $class .'" id="panel2-' . $i . '">' . do_shortcode( $content ) . '</div>';
		}
		add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );
	///TAB PANEL

	// ACCORDION
		function frn_foundation_accordion( $atts, $content = null ) {
			return '<dl class="sl_accordion accordion" data-accordion>' . do_shortcode ( $content ) . '</dl>';
		}
		add_shortcode ('accordion', 'frn_foundation_accordion' );
	///ACCORDION

	// ACCORDION ITEM
		function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'title'		=> ''
				), $atts );
			static $i = 0;
		/*	if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}*/
			$i++;
			$value = '<dd><a href="#panel'. $i . '">' . esc_attr($specs['title'] ) . '</a><div id="panel'. $i .'" class="sl_content content">' . do_shortcode( $content ) . '</div></dd>';

			return $value;
		}

		add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );
	///ACCORDION ITEM


///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK

	// DIVIDER
		function line_divider() {

			return '<div class="sl_divider divider clearfix"></div>';

		}
		add_shortcode( 'divider', 'line_divider' );
	///DIVIDER

	// CALLOUT
		function callout_box ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
				), $atts );
			return '<div class="sl_callout callout ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout', 'callout_box' );
	///CALLOUT

	// CALLOUT CARD
		function callout_card ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				'img'		=> ''
				), $atts );
			return '<div class="sl_callout-card callout-card ' . esc_attr($specs['style'] ) . '"><div class="card-media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="card-content">' . do_shortcode ( $content ) . '</div></div>';
		}
		add_shortcode ('callout-card', 'callout_card' );
	///CALLOUT CARD

	// LARGE TEXT
		function large_text ( $atts, $content = null ) {
			return '<div class="sl_callout-text callout-text">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('large_text', 'large_text' );
	///LARGE TEXT

	// CALLOUT EQUALIZER
		function callout_box_equalizer ( $atts, $content = null ) {
			return '<div class="sl_callout callout equalizer" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout_equalizer', 'callout_box_equalizer' );
	///CALLOUT EQUALIZER

	// MAP
		function map_container ( $atts, $content = null ) {
			return '<div class="map-container">' . $content . '</div>';
		}
		add_shortcode ('map', 'map_container' );
	///MAP

	// CONTAINER
		function div_container ( $atts, $content = null ) {
		    $specs = shortcode_atts( array(
		        'class'     => '',
		        ), $atts );
		    return '<div class="sl_container container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('container', 'div_container' );
	///CONTAINER

	//YOUTUBE EMBED IFRAME
function sl_embed( $atts, $content = null ) {
	$specs = shortcode_atts( array(
	'src' => ''
	), $atts );
	return '<iframe width="560" height="315" src="" data-src=' . esc_attr($specs['src'] ) . ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	}
	add_shortcode ('embed', 'sl_embed' );
	///EMBED

	// EMAIL
	function sl_email ($atts) {
	    $specs = shortcode_atts( array(
	        'heading'     => 'Subscribe to our email list',
			'campaign'     => '',
			'not_content' => '',
			), $atts );

	    // $content = ob_get_clean();

	    static $i = 0;
		$i++;

		ob_start ();
		?><div class="sl_card sl_card--email">
		    <h3 class="sl_email_header"><?php echo esc_attr($specs['heading'] )?></h3>
			<form action="https://foundationsrecoverynetwork.us5.list-manage.com/subscribe/post?u=a517fa2b2970f6fb110a5a1dc&amp;id=3675106f6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate sl_form sl_form--email" target="_blank" novalidate>
				<div class="sl_form__multiselect">
					<div id="sl_form__multiselect__field--<?php echo $i?>" class="sl_form__multiselect__field" tabindex="0">
						<span id="sl_who-input--<?php echo $i?>">I am a...</span>
					</div>
					<div id="sl_form__multiselect__options--<?php echo $i?>" class="mc-field-group input-group sl_form__multiselect__options">
				    	<label for="mce-group[22101]-22101-0--<?php echo $i?>"><input type="checkbox" value="1" name="group[22101][1]" id="mce-group[22101]-22101-0--<?php echo $i?>">I am seeking information for myself</label>
						<label for="mce-group[22101]-22101-1--<?php echo $i?>"><input type="checkbox" value="2" name="group[22101][2]" id="mce-group[22101]-22101-1--<?php echo $i?>">I am seeking information for a loved one</label>
						<label for="mce-group[22101]-22101-2--<?php echo $i?>"><input type="checkbox" value="4" name="group[22101][4]" id="mce-group[22101]-22101-2--<?php echo $i?>">I am an alumni or in recovery</label>
						<label for="mce-group[22101]-22101-3--<?php echo $i?>"><input type="checkbox" value="8" name="group[22101][8]" id="mce-group[22101]-22101-3--<?php echo $i?>">I am an industry professional</label>
					</div>
				</div>
				<input type="email" value="" placeholder="Email" name="EMAIL" class="required email sl_form__input" id="mce-EMAIL--<?php echo $i?>">

				<!-- Passing Source URL -->
				<input style="display: none;" type="text" value="<?php echo get_permalink() ?>" name="URL" class="" id="mce-URL">

				<!-- Passing Campaign Name -->
				<input style="display: none;" type="text" value="<?php echo esc_attr($specs['campaign'])?>" name="MCMPGN" class="" id="mce-MCMPGN">

				<!-- Passing Facility Group -->
				<div style="display: none;" class="mc-field-group input-group">
					<input type="checkbox" value="256" name="group[22145][256]" id="mce-group[22145]-22145-0"><label for="mce-group[22145]-22145-0">Black Bear Lodge</label>
					<input type="checkbox" value="512" name="group[22145][512]" id="mce-group[22145]-22145-1"><label for="mce-group[22145]-22145-1">Talbott Recovery</label>
					<input type="checkbox" value="1024" name="group[22145][1024]" id="mce-group[22145]-22145-2"><label for="mce-group[22145]-22145-2">Skywood Recovery</label>
					<input type="checkbox" value="2048" name="group[22145][2048]" id="mce-group[22145]-22145-3" checked><label for="mce-group[22145]-22145-3">The Oaks at La Paloma</label>
					<input type="checkbox" value="4096" name="group[22145][4096]" id="mce-group[22145]-22145-4"><label for="mce-group[22145]-22145-4">Michael's House</label>
				</div>

				<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				<div style="position: absolute; left: -5000px;" aria-hidden="true">
					<input type="text" name="b_a517fa2b2970f6fb110a5a1dc_3675106f6e" tabindex="-1" value="">
				</div>
				<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="sl_button sl_button--small">
			</form>
			<div id="mce-responses">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[15]='MMERGE15';ftypes[15]='zip';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';fnames[6]='ACCT_OWNER';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';fnames[9]='MMERGE9';ftypes[9]='text';fnames[10]='MMERGE10';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='MMERGE12';ftypes[12]='text';fnames[14]='MCMPGN';ftypes[14]='text';fnames[8]='URL';ftypes[8]='text';fnames[13]='MMERGE13';ftypes[13]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
			<!--End mc_embed_signup-->
	    </div><?php

		$content = ob_get_clean();
		
		if ($specs["not_content"] != "") {
			return do_shortcode($content);
		} else {
			return '[shortcode_unautop]'. do_shortcode($content) .'[/shortcode_unautop]';
		}

	}
	add_shortcode( 'email', 'sl_email' );

		//CTA SHORTCODE
		function oaks_cta_shortcode ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style' => '',
			), $atts );
			return '
			<div class="sl_cta sl_cta--'. esc_attr($specs['style'] ) . '">
				<div class="row">
					<div class="column medium-6">
						<div class="sl_cta__text">
						   <p>Start the Journey Today!</p>'
						   . do_shortcode('[frn_phone ga_phone_location="Phone Clicks in CTA Shortcode"]') .
						'</div>
					</div>
					<div class="column medium-6">
						<div class="sl_cta__button_group">'
							. do_shortcode('[lhn_inpage button="email" text="Email Us" class="sl_cta__button"]')
							. do_shortcode('[lhn_inpage button="chat" text="Chat With Us" offline_text="remove" class="sl_cta__button"]') .
						'</div>
					</div>
				</div>
			</div>';
		}
	
		add_shortcode ('cta', 'oaks_cta_shortcode' );
		//END CTA SHORTCODE


//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'facility_photo_slider';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';

    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// This function reformats autop inside shortcodes. To turn off auto p inside shortcode, wrap shortcode in [shortcode_unautop]

// Example: return '[shortcode_unautop]'. do_shortcode($content) .'[/shortcode_unautop]';

function reformat_auto_p_tags($content) {
    $new_content = '';
    $pattern_full = '{(\[shortcode_unautop\].*?\[/shortcode_unautop\])}is';
    $pattern_contents = '{\[shortcode_unautop\](.*?)\[/shortcode_unautop\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'reformat_auto_p_tags', 99);
add_filter('widget_text', 'reformat_auto_p_tags', 99);