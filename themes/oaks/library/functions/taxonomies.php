<?php
// -------------------------------  Taxonomies ---------------------------------------


// Staff Location
register_taxonomy(  
	'staff_location',  
	array('staff'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Staff Location',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
); 

// Outpatient Staff Location
register_taxonomy(  
	'outpatient_staff_location',  
	array('outpatient_staff'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Outpatient Staff Location',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
);

?>