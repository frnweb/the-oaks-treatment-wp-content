<!-- Mailchimp Email Form -->
        <div class="sl_footer__email">
            <div class="sl_inner">
                <?php echo do_shortcode('[email heading="Subscribe to our email list" not_content="true" ]'); ?>
            </div>
        </div>
<!-- Mailchimp Email Form -->

<div id="mobile-f-floating" class="show-for-small-only vert-pad-tiny">
    <div class="row">
      <div class="small-12 columns text-center call">
        <span class="copy">
          Get Started Today  
        </span>
        <span class="telephone"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Mobile Floating Footer"]'); ?></span>
      </div>
    </div>
  </div>
<footer id="f-main" itemscope itemprop="organization" itemtype="http://schema.org/Organization">
  <div class="row branding">
    <div class="medium-4 columns text-center call vert-pad-small">
      Confidential & Private<br>
      <span class="telephone" itemprop="telephone"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Footer"]'); ?></span>
    </div>
    <div class="medium-4 columns">
        <div class="logo-footer text-center">
            <img itemprop="logo" data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/oaks-logo-footer.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/oaks-logo-retina-footer.png, (retina)]" alt="The Oaks at La Paloma">
        </div>
    </div>
    <div class="medium-4 columns vert-pad-small">
      <h3 class="text-center">Our Network</h3>
      <ul class="medium-block-grid-4 small-block-grid-2 network">
        <li class="text-center"><a href="http://blackbearrehab.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/bb-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/bb-retina-logo.png, (retina)]" alt="Black Bear Rehab, Georgia" width="67"></a></li>
        <li class="text-center"><a href="http://www.michaelshouse.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/mh-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/mh-retina-logo.png, (retina)]" alt="Michael's House Treatment Center, Palm Springs, CA" width="66"></a></li>
        <li class="text-center"><a href="http://talbottcampus.com//"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/talbott-recovery-logo.png, (default)]" alt="Talbott Recovery, Atlanta, GA" width="76"></a></li> 
        <li class="text-center"><a href="http://skywoodrecovery.com//"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/skywood-logo.png, (default)]" alt="Skywood Recovery, Augusta, MI" width="76"></a></li> 
        <li class="text-center"><a href="http://www.foundationsrecoverynetwork.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/frn-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/frn-retina-logo.png, (retina)]" alt="Foundations Recovery Network, Nashville, TN" width="76"></a></li> 
      </ul>
    </div>
  </div>

  <div class="row menus">
    <div class="medium-3 columns">
      <h4>About Us</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-1', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Resources</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-2', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Quick Links</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-3', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Contact</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-4', 'menu_class' => 'no-bullet') ); ?>
           <a id="bbblink" class="sehzbas" href="http://www.bbb.org/nashville/business-reviews/drug-abuse-and-addiction-info-and-treatment/foundations-recovery-network-in-brentwood-tn-37038606#bbbseal" title="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" style="display: block;position: relative;overflow: hidden; width: 100px; height: 38px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-nashville.bbb.org/logo/sehzbas/foundations-recovery-network-37038606.png" width="200" height="38" alt="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
      <h4>Connect & Share</h4>
      <ul class="social inline-list">
        <li id="facebook"><a href="https://www.facebook.com/lapalomatreatmentcenter"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/fb.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/fb-retina.png, (retina)]" alt="The Oaks Treatment Facebook Page"></a></li>
        <li id="twitter"><a href="https://twitter.com/La_Paloma"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/twt.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/twt-retina.png, (retina)]" alt="The Oaks Twitter Profile"></a></li>
        <li id="linkedin"><a href="https://www.linkedin.com/company/la-paloma-treatment-center/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (retina)]" alt="The Oaks LinkedIn Profile"></a></li>
        <li id="youtube"><a href="https://www.youtube.com/channel/UC2iEx36aTAm3wVrd4mFsR-A"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/youtube.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/youtube-retina.png, (retina)]" alt="Foundations Recovery Network Youtube Channel"></a></li>
      </ul>
    </div>
  </div>
  <div class="row footer-accreditations">
    <div class="columns medium-6 small-6">
      <script src="https://static.legitscript.com/seals/3417925.js"></script>
    </div>
    <div class="columns medium-6 small-6">
      <img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/style/images/accreditations/naatp-stacked-white.svg">
    </div>
  </div>
  <div id="copyright" class="text-center"><span>The Oaks at La Paloma is dedicated to providing the highest level of quality care to our patients. The Joint Commission’s gold seal of approval on our website shows that we have demonstrated compliance to the most stringent standards of performance, and we take pride in our accreditation. The Joint Commission standards deal with organization quality, safety-of-care issues and the safety of the environment in which care is provided. If you have concerns about your care, we would like to hear from you. Please contact us at <?php echo do_shortcode('[frn_phone number="678-251-3100"]'); ?>. If you do not feel that your concerns have been addressed adequately, you may contact The Joint Commission at: Division of Accreditation Operations, Office of Quality Monitoring, The Joint Commission, One Renaissance Boulevard, Oakbrook Terrace, IL 60181, Telephone: <?php echo do_shortcode('[frn_phone number="800-994-6610"]'); ?></span></div>
  <div id="copyright" class="text-center">&copy;<?php echo date("Y ");?><span itemprop="name">The Oaks at La Paloma</span> Treatment Center. All Rights Reserved. <a href="<?php echo get_site_url(); ?>/terms">Terms</a> | <a href="<?php echo do_shortcode('[frn_privacy_url]'); ?>">Privacy Policy</a></div>
  <div id="copyright" class="text-center"><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">2911 Brunswick Road</span>, <span itemprop="addressLocality">Memphis, TN</span> <span itemprop="postalCode">38133</span></span></div>
</footer>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.sidr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.2/js.cookie.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/scripts.js?v=1.02"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>

<div class="sl_overlay" id="sl_overlay">
    <div class="sl_overlay_cta">
		<div class="close_btn"><i class="fa fa-times" aria-hidden="true"></i></div>
        <h3 class="sl_overlay__header">The Oaks at La Paloma Has Moved</h3>
        <p class="sl_overlay__content">Our Memphis-based residential treatment center, The Oaks at La Paloma, has relocated, effective April 1, 2020, to the <a href=" https://lakesidebhs.com/programs/the-oaks-at-lakeside/">Lakeside Behavioral Health System</a> campus in Memphis, TN. The program will now be referred to as <b>The Oaks at Lakeside.</b>
        <br><br>
        Not only is Lakeside BHS a sister facility, and a well-known pillar in the Memphis community, they also have a beautiful 37-acre campus that features a 345-bed facility designed to make recovery an accessible, effective reality in the lives of our patients.
        <br><br>
        Our Memphis-based intensive outpatient program, <b>Foundations Memphis</b> will remain at its current location in East Memphis, while affiliated as a program of Lakeside BHS.
        <br><br>
        With this consolidation, we will offer a full continuum of patient care – including acute care, detox, residential treatment, and outpatient programs – and together be a more complete resource to the community.
        <br><br>
        It has been our privilege to serve the Memphis community for the past 15 years, and we look forward to continuing to do so in our new home at <a href=" https://lakesidebhs.com/programs/the-oaks-at-lakeside/">Lakeside</a>! To find out more about The Oaks at Lakeside’s residential and outpatient treatment programs, call us at <?php echo do_shortcode('[frn_phone class="phone-content-overlay" ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?>.
        <br><br>
        Sincerely,
        <br><br>
        The Oaks at La Paloma and Lakeside Behavioral Health System Leadership Team</p>

        <div class="sl_overlay__divider divider clearfix"></div>
        
        <div class="sl_overlay__contact">
          <div class="sl_overlay__btn">
            <a href=" https://lakesidebhs.com/programs/the-oaks-at-lakeside/" class="button small round">Visit Lakeside</a>
          </div>
          <div class="sl_overlay__phone">
              <span>Call us today:</span><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?>
          </div>
        </div>
        <div class="sl_overlay__request">
				<p>To request medical records please contact UHS-NRO Records Department. Fax a copy of the completed/signed ROI form for <a href="/wp-content/uploads/805-The-Oaks-at-laPaloma-ROI-Form.pdf" class="overlay_pdf">the Oaks at La Paloma</a> or for <a href="/wp-content/uploads/814-Foundation-Memphis-ROI-Form.pdf" class="overlay_pdf">Foundations Memphis</a> to FAX# <a style="font-style:initial;" href="tel:615-997-1200" class="phone-content-overlay">(615) 997-1200</a> or it can be emailed to <a class="phone-content-overlay" href="mailto:nrorecordsrequests@uhsinc.com">nrorecordsrequests@uhsinc.com</a>. If you need additional guidance on medical records, please call <a href="tel:615-312-5834" class="phone-content-overlay">(615) 312-5834</a>.</p>
			</div>
    </div><!-- end cta -->
</div><!-- end sl_overlay -->

<script>

$(document).ready(function() {

var date = new Date();
 var minutes = 60;
 date.setTime(date.getTime() + (minutes * 60 * 1000));

// Get the modal
var modal = document.getElementById("sl_overlay");

// Get the <span> element that closes the modal
var btn = document.getElementsByClassName("close_btn")[0];

$(document.body).addClass('modal-is-open');

if (!Cookies.get('modal_shown')) {
  modal.style.display = 'block';
  $(document.body).addClass('modal-is-open');
} else {
  modal.style.display = 'none';
  $(document.body).removeClass('modal-is-open');
}

function closeModal(){
  $.cookie('modal_shown', 'yes', { expires: date, path: '/' });
  modal.style.display = 'none';
  $(document.body).removeClass('modal-is-open');
}

// When the user clicks on <span> (x), close the modal
btn.onclick = function() {
  closeModal();
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    closeModal();
  }
}

});

//add close for COVID banner without Foundation support
var closebtn = document.getElementById('close-btn')
var banner = document.getElementById('sl_notification-bar');

	closebtn.onclick = function(event) {
    banner.style.display = "none";
	}
</script>

</body> 
</html>
